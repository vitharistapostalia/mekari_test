require 'selenium-webdriver'
driver = Selenium::WebDriver.for :chrome
String searchBox="testing@test.com";

Given("Open the amazon page") do
    driver.navigate.to "https://www.amazon.com/"
  end

  Given("click on Sign In tab") do
    driver.find_element(:xpath,'//*[@id="nav-link-accountList"]/span[1]').click();
    # driver.find_element(:xpath,'//span[contains(text(),\'Hello, Sign in\')]').click
  end
  
  Given("input the Email") do
    driver.find_element(:name,'email').send_key("testing@gmail.test");
  end

  Then("Verify message error login present") do
    driver.find_element(:xpath,'//div[@id=\'auth-error-message-box\']//div[@class=\'a-box-inner a-alert-container\']').display
  end

  Then("click Login button") do
    driver.find_element(:id,'continue').click
    sleep(2)
  end

  
  Then("closing the browser") do
    driver.quit
  end
  
  