require 'selenium-webdriver'
driver = Selenium::WebDriver.for :chrome
String searchBox="testing@test.com";


Given("Open the Amazon Page") do
    driver.navigate.to "https://www.amazon.com/"
    sleep(3)
  end
  
  When("Sign Up Amazon") do
    # driver.find_element(:xpath,'//a[@id=\'nav-link-accountList\']//span[@class=\'nav-icon nav-arrow\']').click
    driver.find_element(:xpath,'//a[contains(@href,\'https://www.amazon.com/ap/register?\')]').click
  end

  And("Input the All Fields") do
    driver.find_element(:name,'customerName').send_key("Vitha");
    driver.find_element(:name,'email').send_key("testing@gmail.test");
    driver.find_element(:id,'ap_password').send_key("12345678");
    driver.find_element(:id,'ap_password_check').send_key("12345678");
  end
  
  And("Click Create Your Amazon Account") do
    driver.find_element(:id,'continue').click
    sleep(2)
  end

  Then("Verify message error present") do
    driver.find_element(:xpath,'//div[@id=\'auth-error-message-box\']//div[@class=\'a-box-inner a-alert-container\']').display
  end
